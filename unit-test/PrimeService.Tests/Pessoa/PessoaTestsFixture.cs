using Bogus;

namespace PrimeService.Tests;

public class PessoaTestsFixture
{
    public Pessoa GenerateUnderAgePerson()
    {
        return new Faker<Pessoa>("pt_BR").CustomInstantiator(f => new Pessoa(){
            Age = f.Random.Int(10,18)
        });
    }

    public Pessoa GenerateOverAgePerson()
    {
        return new Faker<Pessoa>("pt_BR").CustomInstantiator(f => new Pessoa(){
            Age = f.Random.Int(18,40)
        });
    }
}
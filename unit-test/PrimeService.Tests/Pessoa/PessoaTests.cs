namespace PrimeService.Tests;

public class PessoaTests : IClassFixture<PessoaTestsFixture>
{
    private readonly PessoaTestsFixture _fixture;

    public PessoaTests(PessoaTestsFixture fixture)
    {
        _fixture = fixture;
    }

    [Fact]
    public void Validate_WhenPersonMayPayMcToyWithDiscount_ShouldReturnTrue()
    {
        var pessoa = _fixture.GenerateUnderAgePerson();

        var exception = pessoa.MacToyValidateDiscount();

        Assert.True(exception);
    }

    [Fact]
    public void Validate_WhenPersonMayNotPayMcToyWithDiscount_ShouldReturnFalse()
    {
        var pessoa = _fixture.GenerateOverAgePerson();

        var exception = pessoa.MacToyValidateDiscount();

        Assert.False(exception);
    }
}